
//
//  Define_Qin.h
//  HealthChat
//
//  Created by Qinge on 15/5/18.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#ifndef HealthChat_Define_Qin_h
#define HealthChat_Define_Qin_h

#define ScreenRectHeight [UIScreen mainScreen].applicationFrame.size.height
#define ScreenRectWidth [UIScreen mainScreen].applicationFrame.size.width

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "UIViewController+HUD.h"
#import "StoryBoardUtilities.h"
#import "RegularJudgeClass.h"
#import "YTKRequest.h"
#import "YTKKeyValueStore.h"
#import "MD5Class.h"
#import "ErrorTextJudge.h"
#import "MJExtension.h"
#import "MJRefresh.h"

// 环信相关
#import "EaseMob.h"
#import "TTGlobalUICommon.h"
#import "UIViewController+DismissKeyboard.h"
#import "EMAlertView.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define KNOTIFICATION_LOGINCHANGE @"loginStateChange"
#define CHATVIEWBACKGROUNDCOLOR [UIColor colorWithRed:0.936 green:0.932 blue:0.907 alpha:1]


// ========= 数据库相关 ========
// MainDBName
#define HealthChatDB @"HealthChat"
// 用户信息
#define UserInfo @"UserInfo"

#endif
