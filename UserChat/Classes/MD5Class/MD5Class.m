//
//  MD5Class.m
//  HealthManagement
//
//  Created by Qinge on 15/5/1.
//  Copyright (c) 2015年 guotion. All rights reserved.
//

#import "MD5Class.h"
#import <CommonCrypto/CommonDigest.h>

@implementation MD5Class
// MD5加密
+ (NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), digest );
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}
@end
