//
//  MD5Class.h
//  HealthManagement
//
//  Created by Qinge on 15/5/1.
//  Copyright (c) 2015年 guotion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MD5Class : NSObject
+ (NSString *)md5:(NSString *)str;

@end
