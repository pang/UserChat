//
//  RegularJudgeClass.h
//  移动医疗
//
//  Created by Qinge on 15/4/7.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegularJudgeClass : NSObject
// 1.用户名 - 2.密码 - 3.再次密码（英文、数字组合）
+ (BOOL)CombinationJudge:(NSString *)range str:(NSString *)str;

// 手机号码
+ (BOOL)checkTel:(NSString *)phoneNumber;

// 验证身份证是否合法 // 参数:输入的身份证号
+(BOOL)chk18PaperId:(NSString *)sPaperId;

// 真实姓名
+ (BOOL)isValidateName:(NSString *)name;

// 邮箱正则判断
+ (BOOL)isValidateEmail:(NSString *)email;

// 判断是否有中文
+ (BOOL)IsChinese:(NSString *)str;

@end
