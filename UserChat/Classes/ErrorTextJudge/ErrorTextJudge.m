//
//  ErrorTextJudge.m
//  移动医疗
//
//  Created by Qinge on 15/1/20.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "ErrorTextJudge.h"

@implementation ErrorTextJudge
+ (NSString *)errorJudge:(NSString *)errorCode
{
    if ([errorCode isEqual:@"error-001"]) {
        return ERROR001;
    }
    if ([errorCode isEqual:@"error-002"]) {
        return ERROR002;
    }
    if ([errorCode isEqual:@"error-003"]) {
        return ERROR003;
    }
    if ([errorCode isEqual:@"error-004"]) {
        return ERROR004;
    }
    if ([errorCode isEqual:@"error-005"]) {
        return ERROR005;
    }
    if ([errorCode isEqual:@"error-006"]) {
        return ERROR006;
    }
    if ([errorCode isEqual:@"error-007"]) {
        return ERROR007;
    }
    if ([errorCode isEqual:@"error-008"]) {
        return ERROR008;
    }
    if ([errorCode isEqual:@"error-009"]) {
        return ERROR009;
    }
    if ([errorCode isEqual:@"error-010"]) {
        return ERROR010;
    }
    if ([errorCode isEqual:@"error-011"]) {
        return ERROR011;
    }
    if ([errorCode isEqual:@"error-012"]) {
        return ERROR012;
    }
    if ([errorCode isEqual:@"error-013"]) {
        return ERROR013;
    }
    if ([errorCode isEqual:@"error-014"]) {
        return ERROR014;
    }
    if ([errorCode isEqual:@"error-015"]) {
        return ERROR015;
    }
    if ([errorCode isEqual:@"error-016"]) {
        return ERROR016;
    }
    if ([errorCode isEqual:@"error-017"]) {
        return ERROR017;
    }
    if ([errorCode isEqual:@"error-018"]) {
        return ERROR018;
    }
    if ([errorCode isEqual:@"error-019"]) {
        return ERROR019;
    }
    if ([errorCode isEqual:@"error-020"]) {
        return ERROR020;
    }
    if ([errorCode isEqual:@"error-021"]) {
        return ERROR021;
    }
    if ([errorCode isEqual:@"error-022"]) {
        return ERROR022;
    }
    if ([errorCode isEqual:@"error-023"]) {
        return ERROR023;
    }
    if ([errorCode isEqual:@"error-024"]) {
        return ERROR024;
    }
    if ([errorCode isEqual:@"error-025"]) {
        return ERROR025;
    }
    if ([errorCode isEqual:@"error-026"]) {
        return ERROR026;
    }
    if ([errorCode isEqual:@"error-027"]) {
        return ERROR027;
    }
    if ([errorCode isEqual:@"error-028"]) {
        return ERROR028;
    }
    if ([errorCode isEqual:@"error-029"]) {
        return ERROR029;
    }
    if ([errorCode isEqual:@"error-030"]) {
        return ERROR030;
    }
    if ([errorCode isEqual:@"error-031"]) {
        return ERROR031;
    }
    if ([errorCode isEqual:@"error-032"]) {
        return ERROR032;
    }
    if ([errorCode isEqual:@"error-033"]) {
        return ERROR033;
    }
    if ([errorCode isEqual:@"error-034"]) {
        return ERROR034;
    }
    if ([errorCode isEqual:@"error-035"]) {
        return ERROR035;
    }
    if ([errorCode isEqual:@"error-036"]) {
        return ERROR036;
    }
    if ([errorCode isEqual:@"error-037"]) {
        return ERROR037;
    }
    if ([errorCode isEqual:@"error-038"]) {
        return ERROR038;
    }
    if ([errorCode isEqual:@"error-039"]) {
        return ERROR039;
    }
    if ([errorCode isEqual:@"error-040"]) {
        return ERROR040;
    }
    if ([errorCode isEqual:@"error-041"]) {
        return ERROR041;
    }
    if ([errorCode isEqual:@"error-042"]) {
        return ERROR042;
    }
    if ([errorCode isEqual:@"error-043"]) {
        return ERROR043;
    }
    if ([errorCode isEqual:@"error-044"]) {
        return ERROR044;
    }
    if ([errorCode isEqual:@"error-045"]) {
        return ERROR045;
    }
    if ([errorCode isEqual:@"error-046"]) {
        return ERROR046;
    }
    if ([errorCode isEqual:@"error-047"]) {
        return ERROR047;
    }
    if ([errorCode isEqual:@"error-048"]) {
        return ERROR048;
    }
    if ([errorCode isEqual:@"error-049"]) {
        return ERROR049;
    }
    if ([errorCode isEqual:@"error-050"]) {
        return ERROR050;
    }
    if ([errorCode isEqual:@"error-051"]) {
        return ERROR051;
    }
    if ([errorCode isEqual:@"error-052"]) {
        return ERROR052;
    }
    if ([errorCode isEqual:@"error-053"]) {
        return ERROR053;
    }
    if ([errorCode isEqual:@"error-054"]) {
        return ERROR054;
    }
    if ([errorCode isEqual:@"error-055"]) {
        return ERROR055;
    }
    if ([errorCode isEqual:@"error-056"]) {
        return ERROR056;
    }
    if ([errorCode isEqual:@"error-057"]) {
        return ERROR057;
    }
    if ([errorCode isEqual:@"error-058"]) {
        return ERROR058;
    }
    if ([errorCode isEqual:@"error-059"]) {
        return ERROR059;
    }
    if ([errorCode isEqual:@"error-060"]) {
        return ERROR060;
    }
    if ([errorCode isEqual:@"error-061"]) {
        return ERROR061;
    }
    if ([errorCode isEqual:@"error-062"]) {
        return ERROR062;
    }
    if ([errorCode isEqual:@"error-063"]) {
        return ERROR063;
    }
    if ([errorCode isEqual:@"error-064"]) {
        return ERROR064;
    }
    if ([errorCode isEqual:@"error-065"]) {
        return ERROR065;
    }
    if ([errorCode isEqual:@"error-066"]) {
        return ERROR066;
    }
    if ([errorCode isEqual:@"error-067"]) {
        return ERROR067;
    }
    if ([errorCode isEqual:@"error-068"]) {
        return ERROR068;
    }
    if ([errorCode isEqual:@"error-069"]) {
        return ERROR069;
    }
    if ([errorCode isEqual:@"error-070"]) {
        return ERROR070;
    }
    if ([errorCode isEqual:@"error-071"]) {
        return ERROR071;
    }
    if ([errorCode isEqual:@"error-072"]) {
        return ERROR072;
    }
    if ([errorCode isEqual:@"error-073"]) {
        return ERROR073;
    }
    if ([errorCode isEqual:@"error-074"]) {
        return ERROR074;
    }
    if ([errorCode isEqual:@"error-075"]) {
        return ERROR075;
    }
    if ([errorCode isEqual:@"error-076"]) {
        return ERROR076;
    }
    if ([errorCode isEqual:@"error-077"]) {
        return ERROR077;
    }
    if ([errorCode isEqual:@"error-078"]) {
        return ERROR078;
    }
    if ([errorCode isEqual:@"error-079"]) {
        return ERROR079;
    }
    if ([errorCode isEqual:@"error-080"]) {
        return ERROR080;
    }
    if ([errorCode isEqual:@"error-081"]) {
        return ERROR081;
    }
    return @"未知错误信息";
    
}

@end
