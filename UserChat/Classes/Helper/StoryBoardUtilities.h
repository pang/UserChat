//
//  StoryBoardUtilities.h
//  HealthChat
//
//
//  Created by Qinge on 15/5/15.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface StoryBoardUtilities : NSObject

/**
 *  获取controller
 *
 *  @param storyboardName  故事版名
 *  @param class          controller
 *
 *  @return 返回controller
 */
+ (UIViewController*)viewControllerForStoryboardName:(NSString*)storyboardName className:(id)classname;

@end
