//
//  UIColor+HexString.h
//  HealthChat
//
//  Created by Qinge on 15/5/15.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)
/**
 *  十六进制的颜色转换为UIColor
 *
 *  @param color   十六进制的颜色
 *
 *  @return   UIColor
 */
+ (UIColor *)colorwithHexString:(NSString *)color;

@end
