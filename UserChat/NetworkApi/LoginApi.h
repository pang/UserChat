//
//  LoginApi.h
//  HealthChat
//
//  Created by Qinge on 15/5/18.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginApi : YTKRequest
/**
 *  用户登录接口
 */

- (instancetype)initWithAccount:(NSString *)account password:(NSString *)password;

@end
