//
//  RegisterApi.m
//  HealthChat
//
//  Created by Qinge on 15/5/18.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "RegisterApi.h"

@implementation RegisterApi {
    NSString *_account;
    NSString *_password;
}


- (instancetype)initWithAccount:(NSString *)account password:(NSString *)password
{
    self = [super init];
    if (self) {
        
        _account = account;
        _password = password;
    }
    return self;
}



- (NSString *)requestUrl
{
    return @"/regis_doctor.do";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPost;
}

- (id)requestArgument {
    
    return @{
             @"account": _account,
             @"password": _password
             };
}


@end
