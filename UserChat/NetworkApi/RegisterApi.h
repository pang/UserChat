//
//  RegisterApi.h
//  HealthChat
//
//  Created by Qinge on 15/5/18.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "YTKRequest.h"

@interface RegisterApi : YTKRequest
/**
 *  用户注册接口
 */
- (instancetype)initWithAccount:(NSString *)account password:(NSString *)password;


@end
