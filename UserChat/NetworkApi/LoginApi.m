//
//  LoginApi.m
//  HealthChat
//
//  Created by Qinge on 15/5/18.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "LoginApi.h"

@implementation LoginApi {
    NSString *_account;
    NSString *_password;
}

- (instancetype)initWithAccount:(NSString *)account password:(NSString *)password
{
    self = [super init];
    if (self) {
        _account = account;
        _password = password;
    }
    return self;
}

- (NSString *)requestUrl
{
    return [NSString stringWithFormat:@"/login.do?user=%@&password=%@&token=&channelId=&baiduId=",_account, _password];
}

@end
