//
//  AppDelegate.m
//  HealthChat
//
//  Created by Qinge on 15/5/15.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+EaseMob.h"
#import "LoginViewController.h"
#import "DoctorMainViewController.h"
#import "YTKNetworkConfig.h"
#import "BaseNavigationViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    // 设置接口地址
    [self setNetworkBaseUrl];

    // 加入通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadDoctorMainVC) name:@"LoadDoctorMainVCNotification" object:nil];

    // 获取UserInfo缓存
    YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:HealthChatDB];
    NSDictionary *queryDict = [store getObjectById:UserInfo fromTable:UserInfo];
    if (queryDict) {
        [self loadDoctorMainVC];
    } else {
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"LoginSB" bundle:nil];
        LoginViewController *loginVC = [storyBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];

        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:loginVC];
        nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.window.rootViewController = loginVC;
    }
    
    // 初始化环信SDK，详细内容在AppDelegate+EaseMob.m 文件中
    [self easemobApplication:application didFinishLaunchingWithOptions:launchOptions];
    
    [self.window makeKeyAndVisible];

    return YES;
}

- (void)setNetworkBaseUrl
{
    YTKNetworkConfig *config = [YTKNetworkConfig sharedInstance];
    config.baseUrl = kBaseUrl;
}

- (void)loadDoctorMainVC
{
    DoctorMainViewController *vc = [[DoctorMainViewController alloc] init];
    BaseNavigationViewController *Nav = [[BaseNavigationViewController alloc] initWithRootViewController:vc];
    self.window.rootViewController = Nav;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
