//
//  Customer.h
//  HealthChat
//
//  Created by Qinge on 15/5/15.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Customer : NSObject

@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *bornDate;
@property (nonatomic, copy) NSString *headImg;
@property (nonatomic, copy) NSString *idCard;
@property (nonatomic, copy) NSString *mail;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *realName;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *userId;

@end
