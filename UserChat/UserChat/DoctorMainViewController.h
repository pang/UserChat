//
//  DoctorMainViewController.h
//  HealthChat
//
//  Created by Qinge on 15/5/15.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorMainViewController : UITabBarController
{
    EMConnectionState _connectionState;
}

- (void)jumpToChatList;

- (void)setupUntreatedApplyCount;

- (void)networkChanged:(EMConnectionState)connectionState;

@end
