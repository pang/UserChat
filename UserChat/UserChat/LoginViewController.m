//
//  LoginViewController.m
//  HealthChat
//
//  Created by Qinge on 15/5/15.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginApi.h"
#import "RegisterApi.h"

@interface LoginViewController ()
{
    NSString *_HXID;
    NSString *_HXPassword;
}
@property (weak, nonatomic) IBOutlet UITextField *mAccountNumber;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *mLoginBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetPasswordBtn;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"登录";
    self.mLoginBtn.layer.masksToBounds = YES;
    self.mLoginBtn.layer.cornerRadius = 4.0f;
    
    
    // 账号
    _mAccountNumber.text = @"123456";
    _password.text = @"123456";

    NSLog(@"");
//    [self getUserInfo];

}

- (void)getUserInfo
{
    // 获取UserInfo缓存
    YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:HealthChatDB];
    NSDictionary *queryDict = [store getObjectById:UserInfo fromTable:UserInfo];
    
    NSLog(@"%@", queryDict);
    if (queryDict) {
        
        // 账号
        _mAccountNumber.text = queryDict[@"userInfo"][@"account"];
        _password.text = queryDict[@"userInfo"][@"password"];

        // 先登录app账号
        [self startLoginAccount:_password.text];
    }

}

// 登录
- (IBAction)loginBtnClick:(id)sender {

    NSString *username = self.mAccountNumber.text;
    NSString *password = self.password.text;
    if ([RegularJudgeClass CombinationJudge:@"{6,20}" str:username]) {
        if ([RegularJudgeClass CombinationJudge:@"{6,20}" str:password]) {
            [self showHudInView:self.view hint:@"登录中..."];
            
            // 先登录app账号
            // 加密
            NSString *passwordTMP = self.password.text;
            NSString *PASS32 = [NSString stringWithFormat:@"%@%@",passwordTMP, NUM32STR];
            NSString *password = [MD5Class md5:PASS32];
            
            [self startLoginAccount:password];
            
        } else {
            TTAlertNoTitle(@"请正确输入6到20位密码");
        }
    } else {
        TTAlertNoTitle(@"请正确输入6到20位账号");
    }
}

// 登录APP账号
- (void)startLoginAccount:(NSString *)password
{
    NSString *account = self.mAccountNumber.text;
    NSLog(@"%@", password);
    
    LoginApi *api = [[LoginApi alloc] initWithAccount:account password:password];
    [api startWithCompletionBlockWithSuccess:^(YTKBaseRequest *request) {
        
        NSData *jsonData = [request.responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",jsonDict);
        
        if (jsonDict[@"userInfo"]) {
//            _HXID = jsonDict[@"userInfo"][@"id"];
//            _HXPassword = jsonDict[@"userInfo"][@"password"];
            _HXID = @"402881854db33061014dbcab03a50003";
            _HXPassword = @"123456";
            NSLog(@"%@-%@", _HXID, _HXPassword);
            // 用户信息缓存
            YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:HealthChatDB];
            [store createTableWithName:UserInfo];
            [store putObject:jsonDict withId:UserInfo intoTable:UserInfo];
            
            // 环信账号登录
            [self chatStartLoginHX];
        } else {
            [self hideHud];
            TTAlertNoTitle([ErrorTextJudge errorJudge:jsonDict[@"stateCode"]]);
        }
        
    } failure:^(YTKBaseRequest *request) {
        [self hideHud];

        TTAlertNoTitle(@"请求失败");
    }];

}

// 环信账号登录
- (void)chatStartLoginHX
{
    //异步登陆账号
    [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:
    _HXID password:_HXPassword completion:
     ^(NSDictionary *loginInfo, EMError *error) {

         if (loginInfo && !error) {
             //设置是否自动登录
             [[EaseMob sharedInstance].chatManager setIsAutoLoginEnabled:YES];
             
             //将2.1.0版本旧版的coredata数据导入新的数据库
             EMError *error = [[EaseMob sharedInstance].chatManager importDataToNewDatabase];
             if (!error) {
                 error = [[EaseMob sharedInstance].chatManager loadDataFromDatabase];
             }
             
             // 发出通知
             [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadDoctorMainVCNotification" object:nil];

             [self hideHud];
         }
         else
         {
             [self hideHud];
             if (error.errorCode == 1010) { // 已经登录直接调APP登录
                 TTAlertNoTitle(@"账号已登录!");
                 
                 // 发出通知
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadDoctorMainVCNotification" object:nil];
                 return ;
             }
             switch (error.errorCode)
             {
                 case EMErrorServerNotReachable:
                     TTAlertNoTitle(@"Connect to the server failed!");
                     break;
                 case EMErrorServerAuthenticationFailure:
                     TTAlertNoTitle(error.description);
                     break;
                 case EMErrorServerTimeout:
                     TTAlertNoTitle(@"Connect to the server timed out!");
                     break;
                 default:
                     TTAlertNoTitle(@"Logon failure");
                     break;
             }
         }
     } onQueue:nil];
}


/**
 *  立即注册
 */
- (IBAction)registerBtnClick:(id)sender {
    [self showHudInView:self.view hint:@"注册中..."];
    
    NSString *account = self.mAccountNumber.text;
    // 加密
    NSString *passwordTMP = self.password.text;
    NSString *PASS32 = [NSString stringWithFormat:@"%@%@",passwordTMP, NUM32STR];
    NSString *password = [MD5Class md5:PASS32];

    RegisterApi *api = [[RegisterApi alloc] initWithAccount:account password:password];
    [api startWithCompletionBlockWithSuccess:^(YTKBaseRequest *request) {
        [self hideHud];
        NSData *jsonData = [request.responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",jsonDict);
        if (jsonDict[@"userInfo"]) {
            TTAlertNoTitle(@"注册成功!");
        } else
            TTAlertNoTitle([ErrorTextJudge errorJudge:jsonDict[@"stateCode"]]);

    } failure:^(YTKBaseRequest *request) {
        [self hideHud];

        TTAlertNoTitle(@"请求失败");
    }];
}

/**
 *  忘记密码
 */
- (IBAction)forgetPasswordBtnClick:(id)sender {

}

//点击空白处结束编辑状态
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
