//
//  QuestionViewController.m
//  HealthChat
//
//  Created by Qinge on 15/5/25.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "QuestionViewController.h"
//#import "FindAllQuestionsRecordByType.h"
//#import "UpdateRestate.h"

#import "QuestionCell.h"
#import "ChatViewController.h"
#import "UIImageView+EMWebCache.h"

#define RowCount 10

@interface QuestionViewController () <UITableViewDataSource, UITableViewDelegate>
{
    int _index;
    NSString *_token;
    NSMutableArray *_allQuestionList;
    BOOL _headerRefreshed;
    int _chooseRow;
}

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation QuestionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    _headerRefreshed = NO;
    
    // 获取UserInfo缓存
    YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:HealthChatDB];
    NSDictionary *queryDict = [store getObjectById:UserInfo fromTable:UserInfo];
    //    _doctorID = [NSString stringWithFormat:@"%@", queryDict[@"userInfo"][@"id"]];
    _token = [NSString stringWithFormat:@"%@", queryDict[@"token"]];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenRectWidth, ScreenRectHeight-93)];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    [self setupRefresh];
}

// 集成刷新控件
- (void)setupRefresh
{
    // 1.上拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
    [self.tableView headerBeginRefreshing];
    
    // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    [self.tableView footerBeginRefreshing];
    
    // 设置文字
    self.tableView.headerPullToRefreshText = @"下拉刷新";
    self.tableView.headerReleaseToRefreshText = @"松开刷新";
    self.tableView.headerRefreshingText = @"正在努力加载...";
    
    self.tableView.footerPullToRefreshText = @"上拉刷新";
    self.tableView.footerReleaseToRefreshText = @"松开刷新";
    self.tableView.footerRefreshingText = @"正在努力加载...";
}

#pragma mark 开始进入刷新状态
- (void)headerRereshing
{
    _index = 1;
    
    [self getQuestionData];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView headerEndRefreshing];
        
        _headerRefreshed = YES;
    });
    
}

- (void)footerRereshing
{
    if (_headerRefreshed == YES) {
        _index ++;
        
        [self getQuestionData];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView footerEndRefreshing];
    });
    
}

// ======================== 请求数据 =====================
- (void)getQuestionData
{
    self.tableView.footerRefreshingText = @"正在努力加载...";
    
//    FindAllQuestionsRecordByType *api = [[FindAllQuestionsRecordByType alloc] initWithIndex:_index Size:RowCount Type:0 Token:_token];
//    [api startWithCompletionBlockWithSuccess:^(YTKBaseRequest *request) {
//        NSData *jsonData = [request.responseString dataUsingEncoding:NSUTF8StringEncoding];
//        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
//        if (_index == 1) {
//            _allQuestionList = [NSMutableArray array];
//        }
//        NSLog(@"%@", jsonDict[@"data"]);
//        for (id obj in jsonDict[@"data"]) {
//            [_allQuestionList addObject:obj];
//        }
//        if (_allQuestionList.count == 0) {
//            _index = 0;
//        }
//        NSLog(@"%@",_allQuestionList);
//
//        [self.tableView reloadData];
//        
//    } failure:^(YTKBaseRequest *request) {
//        _index--;
//        TTAlertNoTitle(@"请求失败");
//    }];
    
}


#pragma mark - 表格视图数据源代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _allQuestionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QuestionCell *cell = [tableView dequeueReusableCellWithIdentifier:[QuestionCell ID]];
    
    // 2.缓存池没有，重新创建
    if (cell == nil) {
        cell = [QuestionCell loadCell];
    }
    
    if (_allQuestionList[indexPath.row]) {
        cell.questionInfo.text = _allQuestionList[indexPath.row][@"ques_info"];
        cell.quesTime.text = _allQuestionList[indexPath.row][@"ques_time"];
        
        NSURL *imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseUrl, _allQuestionList[indexPath.row][@"userinfo"][@"headImg"]]];
        [cell.headImg sd_setImageWithPreviousCachedImageWithURL:imgUrl andPlaceholderImage:nil options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        } completed:^(UIImage *image, NSError *error, EMSDImageCacheType cacheType, NSURL *imageURL) {
        }];
    }
    
    return cell;
}
// 点击 - 查看详情
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    _chooseRow = (int)indexPath.row;
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                    message:@"是否抢答这个问题？"
                                                   delegate:self
                                          cancelButtonTitle:@"取消"
                                          otherButtonTitles:@"确定", nil];
    [alert show];
}
// cell行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}

#pragma AlertDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        // 确定
//        [self showHudInView:self.view hint:@"抢答中..."];
//        UpdateRestate *api = [[UpdateRestate alloc] initWithID:_allQuestionList[_chooseRow][@"id"] State:2 Token:_token];
//        [api startWithCompletionBlockWithSuccess:^(YTKBaseRequest *request) {
//            [self hideHud];
//
//            NSData *jsonData = [request.responseString dataUsingEncoding:NSUTF8StringEncoding];
//            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
////            NSLog(@"%@",jsonDict);
//            if ([[self checkString:jsonDict[@"stateCode"]] isEqual:@"success"]) {
//                [self startChat];
//            }
//        } failure:^(YTKBaseRequest *request) {
//            [self hideHud];
//            TTAlertNoTitle(@"请求失败");
//        }];
    }
}

- (void)startChat {
    NSString *chatroomID = [NSString stringWithFormat:@"%@", [self checkString:_allQuestionList[_chooseRow][@"chatroomid"]]];
    NSString *userName = [self checkString:_allQuestionList[_chooseRow][@"userinfo"][@"name"]];

    [[EaseMob sharedInstance].chatManager asyncJoinPublicGroup:chatroomID completion:^(EMGroup *group, EMError *error) {
        if (!error) {
            NSLog(@"入群成功：%ld人", group.groupOccupantsCount);
            if (group.groupOccupantsCount<2) {
                ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:chatroomID isGroup:YES];
                chatVC.title = userName;
                [self.navigationController pushViewController:chatVC animated:YES];
            } else {
                TTAlertNoTitle(@"该问题已被其他医生抢答");
            }
        } else
            NSLog(@"%@", error.description);
    } onQueue:nil];
}

- (NSString *)checkString:(id)text
{
    if ([text isKindOfClass:[NSString class]]) {
        return text;
    }else if([text isKindOfClass:[NSNumber class]]) {
        return text;
    } else
        return @"";
}

@end
