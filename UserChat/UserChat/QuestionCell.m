
//
//  QuestionCell.m
//  HealthChat
//
//  Created by Qinge on 15/5/25.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "QuestionCell.h"

@implementation QuestionCell

+ (NSString *)ID
{
    return @"questionCell";
}
+ (id)loadCell
{
    return [[NSBundle mainBundle] loadNibNamed:@"QuestionCell" owner:self options:nil][0];
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
