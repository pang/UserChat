//
//  BaseNavigationViewController.m
//  HealthChat
//
//  Created by Qinge on 15/5/19.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import "BaseNavigationViewController.h"

@interface BaseNavigationViewController ()

@end

@implementation BaseNavigationViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_title"] forBarMetrics:UIBarMetricsDefault];
        
        NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil];
        [self.navigationBar setTitleTextAttributes:attributes];
        
        [self.navigationBar setTintColor:[UIColor whiteColor]];
        
        //        [self.navigationBar setBackgroundImage:[self createImageWithColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
        [self.navigationBar setBackIndicatorTransitionMaskImage:[self createImageWithColor:[UIColor clearColor]]];
        [self.navigationBar setShadowImage:[self createImageWithColor:[UIColor clearColor]]];
        
    }
    return self;
}



/**
 * 将UIColor变换为UIImage
 *
 **/
- (UIImage *)createImageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return theImage;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
}

@end
