//
//  main.m
//  UserChat
//
//  Created by Qinge on 15/6/4.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
