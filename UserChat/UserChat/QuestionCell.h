//
//  QuestionCell.h
//  HealthChat
//
//  Created by Qinge on 15/5/25.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionInfo;
@property (weak, nonatomic) IBOutlet UILabel *quesTime;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;


+ (NSString *)ID;
+ (id)loadCell;
@end
