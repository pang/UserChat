//
//  AppDelegate.h
//  HealthChat
//
//  Created by Qinge on 15/5/15.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoctorMainViewController.h"
#import "ApplyViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, IChatManagerDelegate>
{
    EMConnectionState _connectionState;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DoctorMainViewController *mainController;

@end

