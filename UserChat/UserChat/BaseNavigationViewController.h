//
//  BaseNavigationViewController.h
//  HealthChat
//
//  Created by Qinge on 15/5/19.
//  Copyright (c) 2015年 Qinge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationViewController : UINavigationController

@end
