//
//  MJExtensionConfig.m
//  HealthManagement
//
//  Created by Qinge on 15/5/18.
//  Copyright (c) 2015年 guotion. All rights reserved.
//

#import "MJExtensionConfig.h"
#import "Customer.h"

@implementation MJExtensionConfig
/**
 *  这个方法会在MJExtensionConfig加载进内存时调用一次
 */
+ (void)load
{
    // Customer中的ID属性对应着字典中的id
    // 相当于在Customer.m中实现了+replacedKeyFromPropertyName方法
//    [Customer setupReplacedKeyFromPropertyName:^NSDictionary *{
//        return @{@"cardName" : @"cardName",
//                 @"headImgPath" : @"head_img_path",
//                 @"ID" : @"id",
//                 @"name" : @"name",
//                 @"type" : @"type",
//                 @"userID" : @"user_id"
//                 };
//    }];
}
@end
